ITL Ticket Web App | Project Base on Yii Framework
====================================================
Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources


Requeriments
------------
List of tools to prepare the Web App. Make sure do you have a Web Server (Apache or ngix) and PHP 5.4 or superior installed.
* Install Apache, PHP 5.4 or later, MySQL, Git, Composer.
* Best way to add frontend tools is installing Node.js, Bower.

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

INSTALLATION
------------
You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~

TROUBLESHOT TWITTER ACCESS TOKEN
--------------------------------
You will need to get a token that is connected to your GitHub account by following this [simple guide](https://help.github.com/articles/creating-an-access-token-for-command-line-use/) that was provided by GitHub.

But Following this simple command works too:

```bash
composer config --global github-oauth.github.com "my_token"
```


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.

DEPENDENCIES
------------

### User Management
Yii2-user is designed to work out of the box. It means that installation requires minimal steps. Only one configuration step should be taken and you are ready to have user management on the Yii2 website.
Yii2-user can be installed using composer.

```bash
composer require "dektrium/yii2-user:0.9.*@dev"
```
More information and configuration of this Module go to the [Github Repo](https://github.com/dektrium/yii2-user)

### RBAC - Role Base Access Control if Needed
Yii2-Rbac is designed by Dektrium too. Both Module are a perfect fit to control Users and Permissions.

Add Yii2-rbac to the require section of your **composer.json** file:

```js
{
    "require": {
        "dektrium/yii2-rbac": "dev-master"
    }
}
```

And run following command to download extension using **composer**:

```bash
$ php composer.phar update
```

More information and configuration of this Module go to the [Github Repo](https://github.com/dektrium/yii2-rbac)

In order to Migrate the RBAC through yii migrate you need to update the console config file with this code;

```php
    // ...
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        // ...
    ],
```


