<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contactemail */

$this->title = 'Create Contactemail';
$this->params['breadcrumbs'][] = ['label' => 'Contactemail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactemail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
