<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contactemail */

$this->title = 'Update Contactemail: ' . ' ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Contactemail', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id, 'contact_id' => $model->contact_id, 'contact_customer_id' => $model->contact_customer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contactemail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
