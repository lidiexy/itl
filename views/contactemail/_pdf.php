<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Contactemail */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Contactemail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactemail-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contactemail'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'email:email',
        'enabled',
        [
                'attribute' => 'contact.id',
                'label' => 'Contact'
        ],
        'contact_customer_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
