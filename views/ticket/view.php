<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Ticket', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Ticket'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=             
             Html::a(Icon::show('file-pdf-o') . 'PDF',
                ['pdf', 'id' => $model['id']],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>                        
            <?= Html::a('Update', ['update', 'id' => $model->id, 'model_id' => $model->model_id, 'model_manufacture_id' => $model->model_manufacture_id, 'contact_id' => $model->contact_id, 'contact_Customer_id' => $model->contact_Customer_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id, 'model_id' => $model->model_id, 'model_manufacture_id' => $model->model_manufacture_id, 'contact_id' => $model->contact_id, 'contact_Customer_id' => $model->contact_Customer_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        'status',
        [
            'attribute' => 'model.manufacture.name',
            'label' => 'Manufacture',
        ],
        [
            'attribute' => 'model.name',
            'label' => 'Model',
        ],
        [
            'attribute' => 'contact.customer.name',
            'label' => 'Customer',
        ],
        [
            'attribute' => 'contact.fullname',
            'label' => 'Contact',
        ],

    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>