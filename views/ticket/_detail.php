<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

?>
<div class="ticket-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->subject) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        [
            'attribute' => 'model.manufacture.name',
            'label' => 'Manufacture',
        ],
        [
            'attribute' => 'model.name',
            'label' => 'Model',
        ],
        [
            'attribute' => 'contact.customer.name',
            'label' => 'Customer',
        ],
        [
            'attribute' => 'contact.fullname',
            'label' => 'Contact',
        ],
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>