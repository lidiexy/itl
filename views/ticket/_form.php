<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="ticket-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'subject')->textInput(['placeholder' => 'Problem Subject']) ?>

    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

    <!-- Technitian Name & Issue Date -->
    <div class="row">
        <div class="form-group col-xs-12 col-sm-6">
            <?= $form->field($model, 'issue_date')->widget(\kartik\widgets\DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Choose Issue Date'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii:ss'
                ]
            ]) ?>
        </div>
        <div class="form-group col-xs-12 col-sm-6">
            <?= $form->field($model, 'technitian')->textInput(['maxlength' => true, 'placeholder' => 'Technitian']) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-6">
            <!-- Manufacture Selection -->
            <?= $form->field($model, 'model_manufacture_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Manufacture::find()->orderBy('id')->asArray()->all(), 'id', 'name'), [
                'prompt' => 'Chose a manufacture',
                'id' => 'manufacture_id'
            ]); ?>
            <?= Html::a(Icon::show('plus') . 'Add Manufacture', ['/manufacture/create'], ['class' => 'btn btn-warning']); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-6">
            <!-- Model Child of Manufacture -->
            <?= $form->field($model, 'model_id')->widget(\kartik\widgets\DepDrop::classname(), [
                'type'=>\kartik\widgets\DepDrop::TYPE_SELECT2,
                'select2Options'=>[
                    'pluginOptions'=>['allowClear'=>true]
                ],
                'pluginOptions'=>[
                    'depends'=>['manufacture_id'],
                    'placeholder'=>'Select equipment model',
                    'url'=>\yii\helpers\Url::to(['manufacturemodel']),
                    'initialize' => (!$model->isNewRecord),
                ]
            ]); ?>
            <?= Html::a(Icon::show('plus') . 'Add Model', ['/model/create'], ['class' => 'btn btn-warning']); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-xs-12 col-sm-6">
            <!-- Customer Selection -->
            <?= $form->field($model, 'contact_Customer_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Customer::find()->orderBy('id')->asArray()->all(), 'id', 'name'), [
                'prompt' => 'Chose the Customer',
                'id' => 'customer_id'
            ]); ?>
        </div>
        <div class="form-group col-xs-12 col-sm-6">
            <!-- Contact Related to the Customer -->
            <?= $form->field($model, 'contact_id')->widget(\kartik\widgets\DepDrop::classname(), [
                'type'=>\kartik\widgets\DepDrop::TYPE_SELECT2,
                'select2Options'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
                'pluginOptions'=>[
                    'depends'=>['customer_id'],
                    'placeholder'=>'Select the contact',
                    'url'=>\yii\helpers\Url::to(['customercontact']),
                    'initialize' => (!$model->isNewRecord),
                ]
            ]); ?>
            <?= Html::a(Icon::show('plus') . 'Add Contact', ['/contact/create'], ['class' => 'btn btn-warning']); ?>
        </div>
    </div>

    <!-- Right now this feature is not important for this level
    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>
    -->

    <div class="form-group" id="add-manufacture"></div>

    <div class="form-group" id="add-model"></div>

    <div class="form-group" id="add-model"></div>
    <div class="form-group" id="add-model"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'),['index'],['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
