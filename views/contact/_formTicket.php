<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

Pjax::begin();
$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Ticket',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions'=>['hidden'=>true]],
        'subject' => ['type' => TabularForm::INPUT_TEXTAREA],
        'detail' => ['type' => TabularForm::INPUT_TEXTAREA],
        'technitian' => ['type' => TabularForm::INPUT_TEXT],
        'issue_date' => ['type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\DateTimePicker::classname(),
            'options' => [
                'options' => ['placeholder' => 'Choose Issue Date'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'hh:ii:ss dd-M-yyyy'
                ]
            ]
        ],
        'status' => ['type' => TabularForm::INPUT_TEXT],
        'model_id' => [
            'label' => 'Model',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Model::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Model'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'model_manufacture_id' => ['type' => TabularForm::INPUT_TEXT],
        'contact_Customer_id' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowTicket(' . $key . '); return false;', 'id' => 'ticket-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . 'Ticket',
            'type' => GridView::TYPE_INFO,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Row', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTicket()']),
        ]
    ]
]);
Pjax::end();
?>
