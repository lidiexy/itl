<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

$items = [
    [
        'label' => Icon::show('user') . Html::encode('Contact'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
            [
        'label' => Icon::show('envelope') . Html::encode('Contact Email'),
        'content' => $this->render('_dataContactemail', [
            'model' => $model,
            'row' => $model->contactemails,
        ]),
    ],
            [
        'label' => Icon::show('phone-square') . Html::encode('Contact Phone'),
        'content' => $this->render('_dataContactphone', [
            'model' => $model,
            'row' => $model->contactphones,
        ]),
    ],
            [
        'label' => Icon::show('ticket') . Html::encode('Tickets'),
        'content' => $this->render('_dataTicket', [
            'model' => $model,
            'row' => $model->tickets,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
        //        'height' => TabsX::SIZE_TINY
    ],
    'pluginEvents' => [
        "tabsX.click" => "function(e) {setTimeout(function(e){
                if ($('.nav-tabs > .active').next('li').length == 0) {
                    $('#prev').show();
                    $('#next').hide();
                } else if($('.nav-tabs > .active').prev('li').length == 0){
                    $('#next').show();
                    $('#prev').hide();
                }else{
                    $('#next').show();
                    $('#prev').show();
                };
                console.log(JSON.stringify($('.active', '.nav-tabs').html()));
            },10)}",
    ],
]);
?>
