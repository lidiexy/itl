<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

Pjax::begin();
$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Contactphone',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions'=>['hidden'=>true]],
        'type' => [
            'type' => TabularForm::INPUT_DROPDOWN_LIST,
            'items' => [
                'office' => 'Office',
                'mobile' => 'Mobile',
                'fax' => 'Fax',
                'home' => 'Home',
            ]
        ],
        'phone' => ['type' => TabularForm::INPUT_TEXT],
        'contact_customer_id' => ['type' => TabularForm::INPUT_TEXT, 'columnOptions'=>['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowContactphone(' . $key . '); return false;', 'id' => 'contactphone-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . 'Phone Numbers',
            'type' => GridView::TYPE_INFO,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Phone', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowContactphone()']),
        ]
    ]
]);
Pjax::end();
?>
