<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->tickets,
        'key' => function($model){
            return ['id' => $model->id, 'model_id' => $model->model_id, 'model_manufacture_id' => $model->model_manufacture_id, 'contact_id' => $model->contact_id, 'contact_Customer_id' => $model->contact_Customer_id];
        }
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        [
                'attribute' => 'model.name',
                'label' => 'Model'
        ],
        [
            'attribute' => 'model_manufacture_id',
            'label' => 'Manufacture',
            'value' => function($model){
                return $model->model->manufacture->name;
            },
        ],
        [
            'attribute' => 'contact_Customer_id',
            'label' => 'Customer',
            'value' => function($model){
                return $model->contact->customer->name;
            },
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'status',
            'width' => '5%',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'ticket'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
