<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

Pjax::begin();
$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Contactemail',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions'=>['hidden'=>true]],
        'email' => ['type' => TabularForm::INPUT_TEXT],
        'enabled' => [
            'type' => TabularForm::INPUT_DROPDOWN_LIST,
            'items' => [
                1 => 'Yes',
                0 => 'No',
            ]
        ],
        'contact_customer_id' => ['type' => TabularForm::INPUT_TEXT, 'columnOptions'=>['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowContactemail(' . $key . '); return false;', 'id' => 'contactemail-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . 'Contact Emails',
            'type' => GridView::TYPE_INFO,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Email', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowContactemail()']),
        ]
    ]
]);
Pjax::end();
?>
