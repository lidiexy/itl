<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Contact', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contact'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'fullname',
        'jobtitle',
        'street:ntext',
        'city',
        'state',
        'zipcode',
        [
                'attribute' => 'customer.id',
                'label' => 'Customer'
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnContactemail = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'email:email',
        'enabled',
        [
                'attribute' => 'contact.fullname',
                'label' => 'Contact'
        ],
        'contact_customer_id',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContactemail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contactemail']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Contactemail'.' '. $this->title),
        ],
        'columns' => $gridColumnContactemail
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnContactphone = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'type',
        'phone',
        [
                'attribute' => 'contact.fullname',
                'label' => 'Contact'
        ],
        'contact_customer_id',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContactphone,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contactphone']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Contactphone'.' '. $this->title),
        ],
        'columns' => $gridColumnContactphone
    ]);
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnTicket = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        'status',
        [
                'attribute' => 'model.id',
                'label' => 'Model'
        ],
        'model_manufacture_id',
        [
                'attribute' => 'contact.fullname',
                'label' => 'Contact'
        ],
        'contact_Customer_id',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTicket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ticket']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Ticket'.' '. $this->title),
        ],
        'columns' => $gridColumnTicket
    ]);
?>
    </div>
</div>
