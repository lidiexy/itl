<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Contact', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($this->title) . ' | Contact of '. $model->customer->name ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=             
             Html::a(Icon::show('file-pdf-o') . 'PDF',
                ['pdf', 'id' => $model['id']],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>                        
            <?= Html::a('Update', ['update', 'id' => $model->id, 'customer_id' => $model->customer_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id, 'customer_id' => $model->customer_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'fullname',
        'jobtitle',
        'street:ntext',
        'city',
        'state',
        'zipcode',
        [
            'attribute' => 'customer.name',
            'label' => 'Customer',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerContactemail->totalCount){
    $gridColumnContactemail = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'email:email',
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'enabled',
        ],
        //If we want to be specific
        /*[
            'attribute' => 'contact.fullname',
            'label' => 'Contact'
        ],
        [
            'attribute' => 'contact.customer.name',
            'label' => 'Customer',
        ],*/
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContactemail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contactemail']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => Icon::show('envelope') . Html::encode('Emails of'.' '. $this->title),
        ],
        'columns' => $gridColumnContactemail
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerContactphone->totalCount){
    $gridColumnContactphone = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'type',
        'phone',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContactphone,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contactphone']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => Icon::show('phone-square') . Html::encode('Phone Numbers of '.' '. $this->title),
        ],
        'columns' => $gridColumnContactphone
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerTicket->totalCount){
    $gridColumnTicket = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        [
            'attribute' => 'model.name',
            'label' => 'Model'
        ],
        [
            'attribute' => 'model_manufacture_id',
            'label' => 'Manufacture',
            'value' => function($model){
                return $model->model->manufacture->name;
            },
        ],
        [
            'attribute' => 'contact_Customer_id',
            'label' => 'Customer',
            'value' => function($model){
                return $model->contact->customer->name;
            },
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'status',
            'width' => '5%',
        ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTicket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ticket']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Ticket'.' '. $this->title),
        ],
        'columns' => $gridColumnTicket
    ]);
}
?>
    </div>
</div>