<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

?>
<div class="contact-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->fullname) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'fullname',
        'jobtitle',
        'street:ntext',
        'city',
        'state',
        'zipcode',
        [
            'attribute' => 'customer.name',
            'label' => 'Customer',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>