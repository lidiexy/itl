<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

Pjax::begin();
$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
//TODO: Check TabularForm to create better UI to the user
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Ticket',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions'=>['hidden'=>true]],
        'subject' => ['type' => TabularForm::INPUT_TEXT],
        'detail' => ['type' => TabularForm::INPUT_TEXTAREA],
        'technitian' => ['type' => TabularForm::INPUT_TEXT],
        'issue_date' => ['type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\DateTimePicker::classname(),
            'options' => [
                'options' => ['placeholder' => 'Choose Issue Date'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'hh:ii:ss dd-M-yyyy',
                ],
                'size' => '50px'
            ]
        ],
        'model_id' => ['type' => TabularForm::INPUT_TEXT],
        'contact_id' => [
            'label' => 'Contact',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Contact::find()->orderBy('id')->asArray()->all(), 'id', 'fullname'),
                'options' => ['placeholder' => 'Choose Contact'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowTicket(' . $key . '); return false;', 'id' => 'ticket-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => Icon::show('ticket') . 'Ticket',
            'type' => GridView::TYPE_INFO,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add New Ticket', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTicket()']),
        ]
    ]
]);
Pjax::end();
?>
