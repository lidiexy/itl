<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->tickets,
        'key' => function($model){
            return ['id' => $model->id, 'model_id' => $model->model_id, 'model_manufacture_id' => $model->model_manufacture_id, 'contact_id' => $model->contact_id, 'contact_Customer_id' => $model->contact_Customer_id];
        }
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'status',
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        [
            'attribute' => 'model.manufacture.name',
            'label' => 'Manufacture'
        ],
        [
            'attribute' => 'contact.customer.name',
            'label' => 'Customer'
        ],
        [
            'attribute' => 'contact.fullname',
            'label' => 'Contact'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'ticket'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
