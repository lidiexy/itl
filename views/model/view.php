<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */
/* @var $model app\models\Model */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Model', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Model'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=             
             Html::a(Icon::show('file-pdf-o') . 'PDF',
                ['pdf', 'id' => $model['id']],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>                        
            <?= Html::a('Update', ['update', 'id' => $model->id, 'manufacture_id' => $model->manufacture_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id, 'manufacture_id' => $model->manufacture_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'name',
        [
            'attribute' => 'manufacture.name',
            'label' => 'Manufacture',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerTicket->totalCount){
    $gridColumnTicket = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'hidden' => true],
            'subject:ntext',
            'detail:ntext',
            'technitian',
            'issue_date',
            'status',
            [
                'attribute' => 'model.name',
                'label' => 'Model'
        ],
            'model_manufacture_id',
            [
                'attribute' => 'contact.id',
                'label' => 'Contact'
        ],
            'contact_Customer_id',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTicket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ticket']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Ticket'.' '. $this->title),
        ],
        'columns' => $gridColumnTicket
    ]);
}
?>
    </div>
</div>