<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Model */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Model', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Model'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'name',
        [
                'attribute' => 'manufacture.name',
                'label' => 'Manufacture'
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnTicket = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'subject:ntext',
        'detail:ntext',
        'technitian',
        'issue_date',
        'status',
        [
                'attribute' => 'model.name',
                'label' => 'Model'
        ],
        'model_manufacture_id',
        [
                'attribute' => 'contact.id',
                'label' => 'Contact'
        ],
        'contact_Customer_id',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTicket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ticket']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Ticket'.' '. $this->title),
        ],
        'columns' => $gridColumnTicket
    ]);
?>
    </div>
</div>
