<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Contactphone */

$this->title = $model->phone;
$this->params['breadcrumbs'][] = ['label' => 'Contactphone', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactphone-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contactphone'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'type',
        'phone',
        [
                'attribute' => 'contact.id',
                'label' => 'Contact'
        ],
        'contact_customer_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
