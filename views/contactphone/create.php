<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contactphone */

$this->title = 'Create Contactphone';
$this->params['breadcrumbs'][] = ['label' => 'Contactphone', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactphone-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
