<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Contactphone */

$this->title = $model->phone;
$this->params['breadcrumbs'][] = ['label' => 'Contactphone', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactphone-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Contactphone'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
                        
            <?= Html::a('Update', ['update', 'id' => $model->id, 'contact_id' => $model->contact_id, 'contact_customer_id' => $model->contact_customer_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id, 'contact_id' => $model->contact_id, 'contact_customer_id' => $model->contact_customer_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'type',
        'phone',
        [
            'attribute' => 'contact.id',
            'label' => 'Contact',
        ],
        'contact_customer_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>