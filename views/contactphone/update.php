<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contactphone */

$this->title = 'Update Contactphone: ' . ' ' . $model->phone;
$this->params['breadcrumbs'][] = ['label' => 'Contactphone', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->phone, 'url' => ['view', 'id' => $model->id, 'contact_id' => $model->contact_id, 'contact_customer_id' => $model->contact_customer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contactphone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
