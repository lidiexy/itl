<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

Pjax::begin();
$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Contact',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions'=>['hidden'=>true]],
        'fullname' => ['type' => TabularForm::INPUT_TEXT],
        'jobtitle' => ['type' => TabularForm::INPUT_TEXT],
        'street' => ['type' => TabularForm::INPUT_TEXT],
        'city' => ['type' => TabularForm::INPUT_TEXT],
        'state' => ['type' => TabularForm::INPUT_TEXT],
        'zipcode' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowContact(' . $key . '); return false;', 'id' => 'contact-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => Icon::show('user') . 'Contact',
            'type' => GridView::TYPE_INFO,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . ' Add Contact', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowContact()']),
        ]
    ]
]);
Pjax::end();
?>
