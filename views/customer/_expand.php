<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set
$items = [
    [
        'label' => Icon::show('building') . ' ' . Html::encode('Customer'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => Icon::show('users') . ' ' . Html::encode('Contact List'),
        'content' => $this->render('_dataContact', [
            'model' => $model,
            'row' => $model->contacts,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
        //        'height' => TabsX::SIZE_TINY
    ],
    'pluginEvents' => [
        "tabsX.click" => "function(e) {setTimeout(function(e){
                if ($('.nav-tabs > .active').next('li').length == 0) {
                    $('#prev').show();
                    $('#next').hide();
                } else if($('.nav-tabs > .active').prev('li').length == 0){
                    $('#next').show();
                    $('#prev').hide();
                }else{
                    $('#next').show();
                    $('#prev').show();
                };
                console.log(JSON.stringify($('.active', '.nav-tabs').html()));
            },10)}",
    ],
]);
?>
