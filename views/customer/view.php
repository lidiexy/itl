<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Customer'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=             
             Html::a(Icon::show('file-pdf-o') . 'PDF',
                ['pdf', 'id' => $model['id']],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>                        
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'name',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerContact->totalCount){
    $gridColumnContact = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'hidden' => true],
            'fullname',
            'jobtitle',
            [
                'attribute' => 'address',
                'label' => 'Full Address',
                'value' => function($model){
                    return $model->street . ', ' . $model->city . ', ' . $model->state . ' ' . $model->zipcode;
                },
            ],
            [
                'attribute' => 'customer.name',
                'label' => 'Customer'
            ],
        [
            'attribute' => 'active_email',
            'label' => 'Email',
            'value' => function($model){
                //$email = (is_array($model->contactemails) && !empty($model->contactemails)) ? $model->contactemails[0]->email : '';
                $email = $model->enabledEmail;
                return $email;
            },
        ],
        [
            'attribute' => 'active_phone',
            'label' => 'Phone',
            'value' => function($model){
                $phone = (is_array($model->contactphones) && !empty($model->contactphones)) ? $model->contactphones[0]->phone : '';
                return $phone;
            },
        ],

    ];
    echo Gridview::widget([
        'dataProvider' => $providerContact,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contact']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => Icon::show('users') . Html::encode('Contact List'.' '. $this->title),
        ],
        'columns' => $gridColumnContact
    ]);
}
?>
    </div>
</div>