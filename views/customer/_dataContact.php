<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->contacts,
        'key' => function($model){
            return ['id' => $model->id, 'customer_id' => $model->customer_id];
        }
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        [
            'attribute' => 'fullname',
            'label' => 'Full Name'
        ],
        [
            'attribute' => 'jobtitle',
            'label' => 'Job Title'
        ],
        [
            'attribute' => 'address',
            'label' => 'Full Address',
            'value' => function($model){
                return $model->street . ', ' . $model->city . ', ' . $model->state . ' ' . $model->zipcode;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'contact'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
