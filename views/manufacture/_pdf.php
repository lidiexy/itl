<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Manufacture */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Manufacture', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacture-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Manufacture'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'hidden' => true],
        'name',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
    $gridColumnModel = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'hidden' => true],
        'name',
        [
                'attribute' => 'manufacture.name',
                'label' => 'Manufacture'
        ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerModel,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-model']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Model'.' '. $this->title),
        ],
        'columns' => $gridColumnModel
    ]);
?>
    </div>
</div>
