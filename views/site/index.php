<?php
use kartik\icons\Icon;

Icon::map($this); //Load default Font Awesome Icon Set

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>ITL LLC Tickets</h1>

        <p class="lead">Web Application Exercise to replace/port the actual Ticket System.</p>

        <p><a class="btn btn-lg btn-success" href="https://www.linkedin.com/in/lidiexy-alonso-41290636
" title="Lidiexy Alonso LinkedIn">Lidiexy Alonso Version</a></p>

    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><?= \kartik\helpers\Html::a(Icon::show('ticket'), ['/ticket/index']); ?></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Tickets Count</span>
                        <span class="info-box-number"><?= \app\models\base\Ticket::find()->count(); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><?= \kartik\helpers\Html::a(Icon::show('building'), ['/customer/index']); ?></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Customers</span>
                        <span class="info-box-number"><?= \app\models\base\Customer::find()->count(); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><?= \kartik\helpers\Html::a(Icon::show('legal'), ['/manufacture/index']); ?></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Manufactures</span>
                        <span class="info-box-number"><?= \app\models\base\Manufacture::find()->count(); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><?= \kartik\helpers\Html::a(Icon::show('cube'), ['/model/index']); ?></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Equipments</span>
                        <span class="info-box-number"><?= \app\models\base\Model::find()->count(); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>


        </div>

    </div>
</div>
