<?php

namespace app\models;

use Yii;
use \app\models\base\Contactemail as BaseContactemail;

/**
 * This is the model class for table "contactemail".
 */
class Contactemail extends BaseContactemail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['email', 'contact_id', 'contact_customer_id'], 'required'],
            [['email', 'contact_id', 'contact_customer_id'], 'string', 'max' => 255],
            [['enabled'], 'string', 'max' => 1]
        ]);
    }
	
}
