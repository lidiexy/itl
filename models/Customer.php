<?php

namespace app\models;

use Yii;
use \app\models\base\Customer as BaseCustomer;

/**
 * This is the model class for table "customer".
 */
class Customer extends BaseCustomer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['id', 'name'], 'string', 'max' => 255]
        ]);
    }
	
}
