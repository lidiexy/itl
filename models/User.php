<?php

/*
 * This File is Related only to ITL Test Project.
 *
 * (c)Lidiexy Project <http://github.com/lidiexy/itl/>
 *
 * Using the Yii2-User of dektrium
 */

namespace app\models;
use dektrium\user\models\User as BaseUser;

/**
 * @inheritdoc
 * @package app\models
 */
class User extends BaseUser {
    //re-implement some features or add more
}
