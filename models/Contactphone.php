<?php

namespace app\models;

use Yii;
use \app\models\base\Contactphone as BaseContactphone;

/**
 * This is the model class for table "contactphone".
 */
class Contactphone extends BaseContactphone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'phone', 'contact_id', 'contact_customer_id'], 'required'],
            [['type'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 20],
            [['contact_id', 'contact_customer_id'], 'string', 'max' => 255]
        ]);
    }
	
}
