<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Contactemail]].
 *
 * @see Contactemail
 */
class ContactemailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contactemail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contactemail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}