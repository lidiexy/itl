<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "contactphone".
 *
 * @property integer $id
 * @property string $type
 * @property string $phone
 * @property string $contact_id
 * @property string $contact_customer_id
 *
 * @property \app\models\Contact $contact
 */
class Contactphone extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'phone', 'contact_id', 'contact_customer_id'], 'required'],
            [['type'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 20],
            [['contact_id', 'contact_customer_id'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contactphone';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Phone Type',
            'phone' => 'Phone Number',
            'contact_id' => 'Contact',
            'contact_customer_id' => 'Customer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(\app\models\Contact::className(), ['id' => 'contact_id', 'customer_id' => 'contact_customer_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ContactphoneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ContactphoneQuery(get_called_class());
    }
}
