<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "model".
 *
 * @property integer $id
 * @property string $name
 * @property integer $manufacture_id
 *
 * @property \app\models\Manufacture $manufacture
 * @property \app\models\Ticket[] $tickets
 */
class Model extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'manufacture_id'], 'required'],
            [['manufacture_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'model';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Equipment Model',
            'manufacture_id' => 'Manufacture ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacture()
    {
        return $this->hasOne(\app\models\Manufacture::className(), ['id' => 'manufacture_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(\app\models\Ticket::className(), ['model_id' => 'id', 'model_manufacture_id' => 'manufacture_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ModelQuery(get_called_class());
    }
}
