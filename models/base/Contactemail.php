<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "contactemail".
 *
 * @property integer $id
 * @property string $email
 * @property string $enabled
 * @property string $contact_id
 * @property string $contact_customer_id
 *
 * @property \app\models\Contact $contact
 */
class Contactemail extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'contact_id', 'contact_customer_id'], 'required'],
            [['email', 'contact_id', 'contact_customer_id'], 'string', 'max' => 255],
            [['enabled'], 'string', 'max' => 1]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contactemail';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'enabled' => 'Is Enabled ',
            'contact_id' => 'Contact',
            'contact_customer_id' => 'Customer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(\app\models\Contact::className(), ['id' => 'contact_id', 'customer_id' => 'contact_customer_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ContactemailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ContactemailQuery(get_called_class());
    }
}
