<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "ticket".
 *
 * @property string $id
 * @property string $subject
 * @property string $detail
 * @property string $technitian
 * @property string $issue_date
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 * @property integer $model_id
 * @property integer $model_manufacture_id
 * @property string $contact_id
 * @property string $contact_Customer_id
 *
 * @property \app\models\Contact $contact
 * @property \app\models\Model $model
 */
class Ticket extends \yii\db\ActiveRecord
{
    /**
     * DEFINE constants for the Ticket Status
     * new, following up, edited, closed
     */
    const TICKET_CLOSED = 0; //Preparing to Historic Log
    const TICKET_NEW = 1;
    const TICKET_EDITED = 2;
    const TICKET_FOLLOWING = 3; //Preparing to Nested Ticket or Dependent Tickets


    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subject', 'detail', 'model_id', 'model_manufacture_id', 'contact_id', 'contact_Customer_id'], 'required'],
            [['subject', 'detail'], 'string'],
            [['issue_date', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'status', 'model_id', 'model_manufacture_id'], 'integer'],
            [['id', 'contact_id', 'contact_Customer_id'], 'string', 'max' => 255],
            [['technitian'], 'string', 'max' => 50]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'UUID',
            'subject' => 'Subject',
            'detail' => 'Detail',
            'technitian' => 'Technician',
            'issue_date' => 'Issue Date',
            'status' => 'Status',
            'model_id' => 'Equip Model',
            'model_manufacture_id' => 'Equip Manufacture',
            'contact_id' => 'Customer Contact',
            'contact_Customer_id' => 'Customer/Company',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(\app\models\Contact::className(), ['id' => 'contact_id', 'customer_id' => 'contact_Customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(\app\models\Model::className(), ['id' => 'model_id', 'manufacture_id' => 'model_manufacture_id']);
    }

/**
     * @inheritdoc
     * @return type mixed
     */ 
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\TicketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TicketQuery(get_called_class());
    }
}
