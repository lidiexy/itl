<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "manufacture".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \app\models\Model[] $models
 */
class Manufacture extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 45]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacture';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Equipment Manufacture',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasMany(\app\models\Model::className(), ['manufacture_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ManufactureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ManufactureQuery(get_called_class());
    }
}
