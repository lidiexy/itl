<?php

namespace app\models\base;

use Yii;
use mootensai\behaviors\UUIDBehavior;
/**
 * This is the base model class for table "contact".
 *
 * @property string $id
 * @property string $fullname
 * @property string $jobtitle
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $customer_id
 *
 * @property \app\models\Customer $customer
 * @property \app\models\Contactemail[] $contactemails
 * @property \app\models\Contactphone[] $contactphones
 * @property \app\models\Ticket[] $tickets
 */
class Contact extends \yii\db\ActiveRecord
{

    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fullname', 'zipcode', 'customer_id'], 'required'],
            [['street'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['id', 'fullname', 'jobtitle', 'customer_id'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 100],
            [['state'], 'string', 'max' => 45],
            [['zipcode'], 'string', 'max' => 10]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     * @return type mixed
     */
    public function behaviors()
    {
        return [
            [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Contact Name',
            'jobtitle' => 'Job Title',
            'street' => 'Street',
            'city' => 'City',
            'state' => 'State',
            'zipcode' => 'Zip Code',
            'emails' => 'Emails',
            'phones' => 'Phone Numbers',
            'customer_id' => 'Customer/Company',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(\app\models\Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactemails()
    {
        return $this->hasMany(\app\models\Contactemail::className(), ['contact_id' => 'id', 'contact_customer_id' => 'customer_id']);
    }

    /**
     * Return the First Active Email
     * @return string
     */
    public function getEnabledEmail() {
        $emailObj = \app\models\Contactemail::find()->where(['enabled' => '1', 'contact_id' => $this->id, 'contact_customer_id' => $this->customer_id])->one();
        return $emailObj->email;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactphones()
    {
        return $this->hasMany(\app\models\Contactphone::className(), ['contact_id' => 'id', 'contact_customer_id' => 'customer_id']);
    }

    /**
     * Return the First Phone
     * @return string
     */
    public function getFirstPhone() {
        $phoneObj = \app\models\Contactphone::find()->where(['contact_id' => $this->id, 'contact_customer_id' => $this->customer_id])->one();
        return $phoneObj->phone;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(\app\models\Ticket::className(), ['contact_id' => 'id', 'contact_Customer_id' => 'customer_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ContactQuery(get_called_class());
    }
}
