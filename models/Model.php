<?php

namespace app\models;

use Yii;
use \app\models\base\Model as BaseModel;

/**
 * This is the model class for table "model".
 */
class Model extends BaseModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'manufacture_id'], 'required'],
            [['manufacture_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ]);
    }
	
}
