<?php

namespace app\models;

use Yii;
use \app\models\base\Ticket as BaseTicket;

/**
 * This is the model class for table "ticket".
 */
class Ticket extends BaseTicket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['subject', 'detail', 'model_id', 'model_manufacture_id', 'contact_id', 'contact_Customer_id'], 'required'],
            [['subject', 'detail'], 'string'],
            [['issue_date', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'status', 'model_id', 'model_manufacture_id'], 'integer'],
            [['id', 'contact_id', 'contact_Customer_id'], 'string', 'max' => 255],
            [['technitian'], 'string', 'max' => 50]
        ]);
    }
	
}
