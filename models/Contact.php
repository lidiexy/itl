<?php

namespace app\models;

use Yii;
use \app\models\base\Contact as BaseContact;

/**
 * This is the model class for table "contact".
 */
class Contact extends BaseContact
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['fullname', 'zipcode', 'customer_id'], 'required'],
            [['street'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['id', 'fullname', 'jobtitle', 'customer_id'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 100],
            [['state'], 'string', 'max' => 45],
            [['zipcode'], 'string', 'max' => 10]
        ]);
    }
	
}
