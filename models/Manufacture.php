<?php

namespace app\models;

use Yii;
use \app\models\base\Manufacture as BaseManufacture;

/**
 * This is the model class for table "manufacture".
 */
class Manufacture extends BaseManufacture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'string', 'max' => 45]
        ]);
    }
	
}
