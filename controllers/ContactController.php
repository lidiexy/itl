<?php

namespace app\controllers;

use Yii;
use app\models\Contact;
use app\models\Contactemail;
use app\models\Contactphone;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'view', 'create', 'update','delete',
                            'pdf', 'add-contactemail', 'add-contactphone'], //'add-ticket' could be integrated next version
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Contact::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @param string $id
     * @param string $customer_id
     * @return mixed
     */
    public function actionView($id, $customer_id)
    {
        $model = $this->findModel($id, $customer_id);
        $providerContactemail = new \yii\data\ArrayDataProvider([
            'allModels' => $model->contactemails,
        ]);
        $providerContactphone = new \yii\data\ArrayDataProvider([
            'allModels' => $model->contactphones,
        ]);
        $providerTicket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->tickets,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id, $customer_id),
            'providerContactemail' => $providerContactemail,
            'providerContactphone' => $providerContactphone,
            'providerTicket' => $providerTicket,
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contact();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            Yii::$app->session->setFlash('success', [
                'type' => 'success',
                'icon' => 'fa fa-user',
                'message' => Html::encode('Thanks for submit your Customer Contact. We are working on it'),
                'title' => Html::encode('Customer Contact Created'),
            ]); //Notify to the View
            return $this->redirect(['view', 'id' => $model->id, 'customer_id' => $model->customer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $customer_id
     * @return mixed
     */
    public function actionUpdate($id, $customer_id)
    {
        $model = $this->findModel($id, $customer_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            Yii::$app->session->setFlash('success', [
                'type' => 'success',
                'icon' => 'fa fa-user',
                'message' => Html::encode('Thanks for update this Customer Contact.'),
                'title' => Html::encode('Customer Contact Updated'),
            ]); //Notify to the View
            return $this->redirect(['view', 'id' => $model->id, 'customer_id' => $model->customer_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $customer_id
     * @return mixed
     */
    public function actionDelete($id, $customer_id)
    {
        $this->findModel($id, $customer_id)->delete(); //Check deleteWithRelation

        return $this->redirect(['index']);
    }
    
    /**
     * 
     * for export pdf at actionView
     *  
     * @param type $id
     * @return type
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerContactemail = new \yii\data\ArrayDataProvider([
            'allModels' => $model->contactemails,
        ]);
        $providerContactphone = new \yii\data\ArrayDataProvider([
            'allModels' => $model->contactphones,
        ]);
        $providerTicket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->tickets,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerContactemail' => $providerContactemail,
            'providerContactphone' => $providerContactphone,
            'providerTicket' => $providerTicket,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }
    
    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $customer_id
     * @return Contact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $customer_id)
    {
        if (($model = Contact::findOne(['id' => $id, 'customer_id' => $customer_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to load a tabular form grid for Contact email
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddContactemail()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Contactemail');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formContactemail', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to load a tabular form grid for Contact Phone
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddContactphone()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Contactphone');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formContactphone', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to load a tabular form grid for Ticket
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddTicket()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Ticket');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formTicket', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
