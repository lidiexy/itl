<?php

namespace app\controllers;

use Yii;
use app\models\Contactphone;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * ContactphoneController implements the CRUD actions for Contactphone model.
 */
class ContactphoneController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update','delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Contactphone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Contactphone::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contactphone model.
     * @param integer $id
     * @param string $contact_id
     * @param string $contact_customer_id
     * @return mixed
     */
    public function actionView($id, $contact_id, $contact_customer_id)
    {
        $model = $this->findModel($id, $contact_id, $contact_customer_id);
        return $this->render('view', [
            'model' => $this->findModel($id, $contact_id, $contact_customer_id),
        ]);
    }

    /**
     * Creates a new Contactphone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contactphone();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id, 'contact_id' => $model->contact_id, 'contact_customer_id' => $model->contact_customer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contactphone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $contact_id
     * @param string $contact_customer_id
     * @return mixed
     */
    public function actionUpdate($id, $contact_id, $contact_customer_id)
    {
        $model = $this->findModel($id, $contact_id, $contact_customer_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id, 'contact_id' => $model->contact_id, 'contact_customer_id' => $model->contact_customer_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contactphone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $contact_id
     * @param string $contact_customer_id
     * @return mixed
     */
    public function actionDelete($id, $contact_id, $contact_customer_id)
    {
        $this->findModel($id, $contact_id, $contact_customer_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Contactphone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $contact_id
     * @param string $contact_customer_id
     * @return Contactphone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $contact_id, $contact_customer_id)
    {
        if (($model = Contactphone::findOne(['id' => $id, 'contact_id' => $contact_id, 'contact_customer_id' => $contact_customer_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
