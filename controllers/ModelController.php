<?php

namespace app\controllers;

use Yii;
use app\models\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * ModelController implements the CRUD actions for Model model.
 */
class ModelController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'view', 'create', 'update','delete',
                            'pdf', //'add-ticket', when the _formTicket was done.
                        ],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Model models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Model::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Model model.
     * @param integer $id
     * @param integer $manufacture_id
     * @return mixed
     */
    public function actionView($id, $manufacture_id)
    {
        $model = $this->findModel($id, $manufacture_id);
        $providerTicket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->tickets,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id, $manufacture_id),
            'providerTicket' => $providerTicket,
        ]);
    }

    /**
     * Creates a new Model model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Model();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            Yii::$app->session->setFlash('success', [
                'type' => 'success',
                'icon' => 'fa fa-cube',
                'message' => Html::encode('Thanks for submit the Equipment Model. Great Creation.'),
                'title' => Html::encode('Equipment Model Created'),
            ]); //Notify to the View
            return $this->redirect(['view', 'id' => $model->id, 'manufacture_id' => $model->manufacture_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Model model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $manufacture_id
     * @return mixed
     */
    public function actionUpdate($id, $manufacture_id)
    {
        $model = $this->findModel($id, $manufacture_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            Yii::$app->session->setFlash('success', [
                'type' => 'success',
                'icon' => 'fa fa-cube',
                'message' => Html::encode('Thanks for update this Equipment Model.'),
                'title' => Html::encode('Equipment Model Updated'),
            ]); //Notify to the View
            return $this->redirect(['view', 'id' => $model->id, 'manufacture_id' => $model->manufacture_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Model model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $manufacture_id
     * @return mixed
     */
    public function actionDelete($id, $manufacture_id)
    {
        $this->findModel($id, $manufacture_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }
    
    /**
     * 
     * for export pdf at actionView
     *  
     * @param type $id
     * @return type
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerTicket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->tickets,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerTicket' => $providerTicket,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }
    
    /**
     * Finds the Model model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $manufacture_id
     * @return Model the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $manufacture_id)
    {
        if (($model = Model::findOne(['id' => $id, 'manufacture_id' => $manufacture_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to load a tabular form grid for Ticket
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddTicket()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Ticket');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formTicket', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
