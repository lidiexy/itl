<?php

namespace app\controllers;

use Yii;
use app\models\Ticket;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'view', 'create', 'update','delete', 'pdf',
                            'manufacturemodel', 'customercontact'
                        ],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ticket::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ticket model.
     * @param string $id
     * @param integer $model_id
     * @param integer $model_manufacture_id
     * @param string $contact_id
     * @param string $contact_Customer_id
     * @return mixed
     */
    public function actionView($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id)
    {
        $model = $this->findModel($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id);
        return $this->render('view', [
            'model' => $this->findModel($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id),
        ]);
    }

    /**
     * Creates a new Ticket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ticket();

        if ($model->loadAll(Yii::$app->request->post()) && ($model->status = Ticket::TICKET_NEW) && $model->saveAll()) {
            Yii::$app->session->setFlash('success', [
                'type' => 'success',
                'icon' => 'fa fa-ticket',
                'message' => Html::encode('Thanks for submit your Ticket. We are working on it'),
                'title' => Html::encode('Ticket Created'),
            ]); //Notify to the View
            return $this->redirect(['view', 'id' => $model->id, 'model_id' => $model->model_id, 'model_manufacture_id' => $model->model_manufacture_id, 'contact_id' => $model->contact_id, 'contact_Customer_id' => $model->contact_Customer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ticket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param integer $model_id
     * @param integer $model_manufacture_id
     * @param string $contact_id
     * @param string $contact_Customer_id
     * @return mixed
     */
    public function actionUpdate($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id)
    {
        $model = $this->findModel($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id);

        if ($model->loadAll(Yii::$app->request->post()) && ($model->status = Ticket::TICKET_EDITED) && $model->saveAll()) {
            Yii::$app->session->setFlash('success', [
                'type' => 'success',
                'icon' => 'fa fa-ticket',
                'message' => Html::encode('Thanks for update this Ticket. We are looking those changes.'),
                'title' => Html::encode('Ticket Updated'),
            ]); //Notify to the View
            return $this->redirect(['view', 'id' => $model->id, 'model_id' => $model->model_id, 'model_manufacture_id' => $model->model_manufacture_id, 'contact_id' => $model->contact_id, 'contact_Customer_id' => $model->contact_Customer_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param integer $model_id
     * @param integer $model_manufacture_id
     * @param string $contact_id
     * @param string $contact_Customer_id
     * @return mixed
     */
    public function actionDelete($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id)
    {
        $this->findModel($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }
    
    /**
     * 
     * for export pdf at actionView
     *  
     * @param type $id
     * @return type
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Action to load a tabular form grid for Contact
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddContact()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Contact');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('action') == 'load' && empty($row)) || Yii::$app->request->post('action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formContact', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param integer $model_id
     * @param integer $model_manufacture_id
     * @param string $contact_id
     * @param string $contact_Customer_id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $model_id, $model_manufacture_id, $contact_id, $contact_Customer_id)
    {
        if (($model = Ticket::findOne(['id' => $id, 'model_id' => $model_id, 'model_manufacture_id' => $model_manufacture_id, 'contact_id' => $contact_id, 'contact_Customer_id' => $contact_Customer_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Create the JSON from Model to populate the DropDownList.
     * Get the Data From Manufature when ID given
     * @return Json parsed
     */
    public function actionManufacturemodel() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $manufacture_id = $parents[0];
                $out = \app\models\Model::find()->select(['id','name'])->where(['manufacture_id' => $manufacture_id])->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    /**
     * Create the JSON from Model to populate the DropDownList.
     * Get the Data From Manufature when ID given
     * @return Json parsed
     */
    public function actionCustomercontact() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $customer_id = $parents[0];
                $out = \app\models\Contact::find()->select(['id','fullname as name'])->where(['customer_id' => $customer_id])->asArray()->all();
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}
